import logging
import sys
import messages

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

if __name__ == "__main__":
    try:
        with messages.open_channel() as chan:
            for msg in chan:
                print("[msg]: {}".format(msg))

    except KeyboardInterrupt:
        print("Bye!")
