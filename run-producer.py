import logging
import sys
import messages


logging.basicConfig(stream=sys.stdout, level=logging.INFO)


if __name__ == "__main__":
    try:
        with messages.open_channel() as chan:
            while True:
                msg = raw_input("[msg]: ")
                chan.publish(msg)
    except KeyboardInterrupt:
        print("Bye!")
