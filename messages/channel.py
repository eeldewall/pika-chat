import logging
from .connection import connection
from . import EXCHANGE, EXCHANGE_TYPE, QUEUE

LOG = logging.getLogger(__name__)


class OpenChannel(object):
    
    def __init__(self, channel):
        self.channel = channel

    def publish(self, msg):
        self.channel.basic_publish(
            exchange=EXCHANGE,
            routing_key='',
            body=msg
        )

    def __iter__(self):
        for method_frame, properties, body in self.channel.consume(QUEUE):
            yield body
            self.channel.basic_ack(method_frame.delivery_tag)


def declare_exchange(channel):
    LOG.info("Declaring exchange: {}, type: {}".format(
        EXCHANGE, EXCHANGE_TYPE
    ))
    channel.exchange_declare(
        exchange=EXCHANGE,
        type=EXCHANGE_TYPE
    )


def declare_and_bind_queue(channel):
    result = channel.queue_declare(QUEUE, exclusive=False, durable=True)
    channel.queue_bind(
        exchange=EXCHANGE,
        queue=result.method.queue
    )
   

class open_channel(object):

    def __enter__(self):
        LOG.info("Opening channel")
        self.conn = connection()
        channel = self.conn.channel()
        declare_exchange(channel)
        declare_and_bind_queue(channel)
        return OpenChannel(channel)

    def __exit__(self, *args):
        self.conn.close()
        LOG.info("Closing channel")
