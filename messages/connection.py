import pika
import logging
from . import HOST


LOG = logging.getLogger(__name__)


def connection():
    conn = pika.BlockingConnection(
        pika.ConnectionParameters(host=HOST)
    )
    return conn
