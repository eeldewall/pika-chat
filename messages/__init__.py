ROUTING_KEY = "messages"
EXCHANGE = "exchange.messages"
EXCHANGE_TYPE = "fanout"
QUEUE = "queue.messages"
HOST = "192.168.59.103"

from .channel import open_channel
