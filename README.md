Small pub/sub app with python-pika.

Expects boot2docker and some other hardcoded stuff

// Start RabbitMQ
# docker-compose start
# python run-producer.py
# python run-consumer.py
